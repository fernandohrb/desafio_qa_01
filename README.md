+![Alt text](inmetrics.png)
+
+## **Desafio QA - WEB/API** ##
+
+---
+
+##  Desafio 01 ##
+
+1. Sistema Operacional utilizado: Windows vers�o 10
+2. Linguagem: Java / Cucumber / Junit
+
+### Itens necess�rios
+- Importar o projeto utilizando o Eclipse.( Menu File > Import )
+Obs.: Realizado o teste no Eclipe Version: 2019-03 (4.11.0)
+
+---
+
+## Execu��o do Teste ##
+
+A execu��o do teste pode ser realizada via Junit na classe caminho:"OnlineStore\src\cucumber Test\TestRunner.java"
+
+---

+##  Desafio 02 ##
+
+1. Sistema Operacional utilizado: Windows vers�o 10
+2. Linguagem: Java / Rest-Assured
+
+### Itens necess�rios
+- Importar o projeto utilizando o Eclipse.( Menu File > Import )
+Obs.: Realizado o teste no Eclipe Version: 2019-03 (4.11.0)
+
+---
+
+## Execu��o do Teste ##
+
+A execu��o do teste pode ser realizada via Junit na classe caminho:"JavaWebComMaven\src/test/java/\TestAPI\TestAPI.java"
+
+---

