package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.DataTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Assert; 


public class PageRoom {
	
	WebDriver driver;
	
	public PageRoom(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.setDriver(driver);
	}
	
	@FindBy(how = How.NAME,using = "email" )
	private WebElement txtEmail;

	@FindBy(how = How.NAME, using = "password")
	private WebElement txtSenha;
	
	@FindBy(how = How.XPATH,using = "//button[@type='submit']")
	private WebElement btnLogin;
	
	@FindBy(how = How.XPATH,using = "//body[contains(@class,'pace-done')]")
	private WebElement homePage;
	
	
	@FindBy(how = How.NAME,using = "roomstatus")
	private WebElement cboStatus;
	
	@FindBy(how = How.XPATH,using = "//span[contains(text(),'Room Type')]")
	private WebElement cboRommType;
	
	@FindBy(how = How.CLASS_NAME,using = "select2-results")
	private WebElement listaRoomType;
	
	@FindBy(how = How.XPATH,using = "//*[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	private WebElement txtArea;
	
	@FindBy(how = How.NAME,using = "basicprice")
	private WebElement txtBasicprice;
	
	@FindBy(how = How.NAME,using = "quantity")
	private WebElement txtQuantity;
	
	@FindBy(how = How.NAME,using = "minstay")
	private WebElement txtMinStay;
	
	@FindBy(how = How.NAME,using = "adults")
	private WebElement txtMaxAdults;
	
	@FindBy(how = How.NAME,using = "children")
	private WebElement txtMaxChildren;
	
	@FindBy(how = How.NAME,using = "extrabeds")
	private WebElement txtExtrabeds;
	
	@FindBy(how = How.NAME,using = "bedcharges")
	private WebElement txtBedcharges;
	
	@FindBy(how = How.CLASS_NAME,using = "iCheck-helper")
	private WebElement checkTodos;
	
	
	@FindBy(how = How.ID,using = "add")
	private WebElement btnAdd;
	
	public void preencherLogin(String email,String senha)
	{
		//Preenchee os dados de Login
		try {
			txtEmail.sendKeys(email);
			Thread.sleep(1000);
			txtSenha.sendKeys(senha);
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clicarLogin()
	{
		btnLogin.click();		
	}
	
	public void validarHome()

	
	{
	    boolean achou = true;
		try 
		{
			//driver.findElement((By) homePage);
			driver.findElement(By.xpath("//strong[contains(text(),'Dashboard')]"));
		} catch (Exception e) {
		   achou = false;
		}
		
		Assert.assertTrue("A home Page N�o foi carregada com sucesso", achou);
		
	}
	
	public void validarNomeFuncionalidade(String name)
	{
	    boolean achou = true;
		try 
		{
			//driver.findElement((By) homePage);
			driver.findElement(By.xpath("//h3[contains(text(),'"+name+"')]"));
		} catch (Exception e) {
		   achou = false;
		}
		
		Assert.assertTrue("N�o Carregou a Funcionalidade " + name, achou);
		
	}

	public void preencherStatusRoomType() throws InterruptedException
	{
		
		//selecionando Valores aleat�rios Status
		Select status = new Select(cboStatus);
		List<WebElement> lista = status.getOptions();
		Random rand = new Random();
		int index = rand.nextInt(lista.size());
		status.selectByIndex(index);
		
		//selecionando Valores aleat�rios RoomType
		cboRommType.click();
		Thread.sleep(2000);
		List<String> val = Arrays.asList(driver.findElement(By.xpath("//div[@id='select2-drop']//ul[contains(@class,'select2-results')]")).getText().split("\n"));
		Random randLista = new Random();
		int list = randLista.nextInt(val.size());
		String valor = val.get(list);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[contains(text(),'"+valor+"')]")).click();
		Thread.sleep(2000);

		
	}
	
	
	public void preencherCampos(DataTable table) throws InterruptedException
	{
		
		List<Map<String,String>> data = table.asMaps(String.class, String.class);
		//txtArea.sendKeys(data.get(0).get("Room Description"));
		Thread.sleep(2000);
		txtBasicprice.sendKeys(data.get(0).get("Price"));
		Thread.sleep(2000);
		txtQuantity.sendKeys(data.get(0).get("Quantity"));
		Thread.sleep(2000);
		txtMinStay.sendKeys(data.get(0).get("Minimium Stay"));
		Thread.sleep(2000);
		txtMaxAdults.sendKeys(data.get(0).get("MaxAdults"));
		Thread.sleep(2000);
		txtMaxChildren.sendKeys(data.get(0).get("MaxChildren"));
		Thread.sleep(2000);
		txtExtrabeds.sendKeys(data.get(0).get("NoExtraBeds"));
		Thread.sleep(2000);
		txtBedcharges.sendKeys(data.get(0).get("ExtraBedsCharges"));
		
	}
	
	
	public void selecionarAba(String aba)
	{
		driver.findElement(By.xpath("//a[contains(text(),'"+aba+"')]")).click();
	}
	
	public void selecionarTodos()
	{
		
		List<WebElement> listaElementos = driver.findElements(By.xpath("//label[(@class='pointer')]"));
		Random rdn = new Random();
		int num = rdn.nextInt(listaElementos.size());
		listaElementos.get(num).click();
		//checkTodos.click();
	}
	
	public void addRoom()
	{
		btnAdd.click();
	}
	
	public void validarCadastro(String valor)
	{
		String result = driver.findElement(By.xpath("//div[@class='panel-heading']")).getText();
		Assert.assertEquals("O sistema n�o apresentou a tela " + valor, valor, driver.findElement(By.xpath("//div[@class='panel-heading']")).getText());
		//div[@class='panel-heading']
	}
		
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}	
}
