package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import PageObjects.PageRoom;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberTest.TestRunner;

public class Hotel {

	WebDriver driver;
	PageRoom pageRoom;
	
	@Before
	public void setUp()
	{
		System.setProperty("webdriver.gecko.driver", "C:\\dev\\Cucumber_VFINAL\\Cucumber\\libs\\geckodriver.exe");
        driver = new FirefoxDriver();
        pageRoom = new PageRoom(driver);
	}
	/*
	public Hotel()
	{
		System.setProperty("webdriver.gecko.driver", "C:\\driver\\geckodriver.exe");
        driver = new FirefoxDriver();
        login = new PageLogin(driver);
	}
	*/
	
	@Given("^Usuario esteja na pagina \"([^\"]*)\"$")
	public void usuario_esteja_na_pagina(String url) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		//System.setProperty("webdriver.gecko.driver", "C://JavaProject//Cucumber//libs//geckodriver.exe");
       // driver = new FirefoxDriver();

      //Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
      //Launch the Online Store Website
        driver.get(url);
	}
	

	@Given("^preencher os campos Email \"([^\"]*)\" e Password \"([^\"]*)\"$")
	public void preencher_os_campos_Email_e_Password(String email, String senha) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.preencherLogin(email, senha);
	}

	@When("^clicar no botao Login$")
	public void clicar_no_bot_o_Login() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.clicarLogin();
	}

	@Then("^o sistema deve apresentar a HomePage$")
	public void o_sistema_deve_apresentar_a_HomePage() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(10000);
		pageRoom.validarHome();
	}

	@When("^usuario selecionar o menu HOTELS e selecionar a opcao ADD ROOM$")
	public void usuario_selecionar_o_menu_HOTELS_e_selecionar_a_opcao_ADD_ROOM() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//a[@href='#Hotels']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[contains(text(),'Add Room')]")).click();
	}


	@Then("^o sistema deve apresentar a funcionalidade \"([^\"]*)\"$")
	public void o_sistema_deve_apresentar_a_funcionalidade(String name) throws Throwable {
		pageRoom.validarNomeFuncionalidade(name);
	}

	@When("^preencher selecionar os Campos \\[Status\\] e \\[RoomType\\]$")
	public void preencher_selecionar_os_Campos_Status_e_RoomType() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.preencherStatusRoomType();
	}

	@When("^preencher os campos$")
	public void preencher_os_campos(DataTable table) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		Thread.sleep(5000);
		pageRoom.preencherCampos(table);
	}

	@When("^clicar na aba \"([^\"]*)\"$")
	public void clicar_na_aba(String aba) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.selecionarAba(aba);
	}

	@When("^selecionar a opcao Select All$")
	public void selecionar_a_opcao_Select_All() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.selecionarTodos();
	}
	
	@When("^clicar no botao \\[Add\\]$")
	public void clicar_no_botao_Add() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.addRoom();
		Thread.sleep(5000);
	}

	@Then("^o sistema deve apresentar a tela \"([^\"]*)\"$")
	public void o_sistema_deve_apresentar_a_mensagem(String valor) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pageRoom.validarCadastro(valor);
	}
	

	@After
	public void tearDown()
	{
		driver.close();
	}
}
