Feature: Hotel
  #Scenario: Log in com Sucesso.
    #Given Usuario esteja na pagina "https://www.phptravels.net/supplier"
    #And preencher os campos Email "supplier@phptravels.com" e Password "demosupplier"
    #When clicar no botao Login
    #Then o sistema deve apresentar a HomePage
    
  Scenario: Adicionar Quarto de Hotel
    Given Usuario esteja na pagina "https://www.phptravels.net/supplier"
    And preencher os campos Email "supplier@phptravels.com" e Password "demosupplier"
    When clicar no botao Login
    Then o sistema deve apresentar a HomePage
    When usuario selecionar o menu HOTELS e selecionar a opcao ADD ROOM
    Then o sistema deve apresentar a funcionalidade "Add Room"
    When preencher selecionar os Campos [Status] e [RoomType]
    And preencher os campos
	    |Room Description|Price|Quantity|Minimium Stay|MaxAdults|MaxChildren|NoExtraBeds|ExtraBedsCharges|
	    |Quartos com Wifi|20000|2       |10           |5        |3          |5          |1               |
	When clicar na aba "Amenities"
    And selecionar a opcao Select All
    When clicar no botao [Add]
    Then o sistema deve apresentar a tela "ROOMS MANAGEMENT"
    